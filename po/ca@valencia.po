# Catalan translations for malcontent package.
# Copyright (C) 2020 THE malcontent'S COPYRIGHT HOLDER
# This file is distributed under the same license as the malcontent package.
# Automatically generated, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: malcontent\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-10 12:04+0100\n"
"PO-Revision-Date: 2020-04-15 13:42+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: accounts-service/com.endlessm.ParentalControls.policy.in:4
msgid "Change your own app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:5
msgid "Authentication is required to change your app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:14
msgid "Read your own app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:15
msgid "Authentication is required to read your app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:24
msgid "Change another user’s app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:25
msgid "Authentication is required to change another user’s app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:34
msgid "Read another user’s app filter"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:35
msgid "Authentication is required to read another user’s app filter."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:44
msgid "Change your own session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:45
msgid "Authentication is required to change your session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:54
msgid "Read your own session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:55
msgid "Authentication is required to read your session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:64
msgid "Change another user’s session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:65
msgid "Authentication is required to change another user’s session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:74
msgid "Read another user’s session limits"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:75
msgid "Authentication is required to read another user’s session limits."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:84
msgid "Change your own account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:85
msgid "Authentication is required to change your account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:94
msgid "Read your own account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:95
msgid "Authentication is required to read your account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:104
msgid "Change another user’s account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:105
msgid "Authentication is required to change another user’s account info."
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:114
msgid "Read another user’s account info"
msgstr ""

#: accounts-service/com.endlessm.ParentalControls.policy.in:115
msgid "Authentication is required to read another user’s account info."
msgstr ""

#: libmalcontent/app-filter.c:694
#, c-format
msgid "App filter for user %u was in an unrecognized format"
msgstr ""

#: libmalcontent/app-filter.c:725
#, c-format
msgid "OARS filter for user %u has an unrecognized kind ‘%s’"
msgstr ""

#: libmalcontent/manager.c:283 libmalcontent/manager.c:412
#, c-format
msgid "Not allowed to query app filter data for user %u"
msgstr ""

#: libmalcontent/manager.c:288
#, c-format
msgid "User %u does not exist"
msgstr ""

#: libmalcontent/manager.c:394
msgid "App filtering is globally disabled"
msgstr ""

#: libmalcontent/manager.c:777
msgid "Session limits are globally disabled"
msgstr ""

#: libmalcontent/manager.c:795
#, c-format
msgid "Not allowed to query session limits data for user %u"
msgstr ""

#: libmalcontent/session-limits.c:306
#, c-format
msgid "Session limit for user %u was in an unrecognized format"
msgstr ""

#: libmalcontent/session-limits.c:328
#, c-format
msgid "Session limit for user %u has an unrecognized type ‘%u’"
msgstr ""

#: libmalcontent/session-limits.c:346
#, c-format
msgid "Session limit for user %u has invalid daily schedule %u–%u"
msgstr ""

#. TRANSLATORS: This is the formatting of English and localized name
#. of the rating e.g. "Adults Only (solo adultos)"
#: libmalcontent-ui/gs-content-rating.c:75
#, c-format
msgid "%s (%s)"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:209
msgid "General"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:218
msgid "ALL"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:222
#: libmalcontent-ui/gs-content-rating.c:485
msgid "Adults Only"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:224
#: libmalcontent-ui/gs-content-rating.c:484
msgid "Mature"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:226
#: libmalcontent-ui/gs-content-rating.c:483
msgid "Teen"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:228
#: libmalcontent-ui/gs-content-rating.c:482
msgid "Everyone 10+"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:230
#: libmalcontent-ui/gs-content-rating.c:481
msgid "Everyone"
msgstr ""

#: libmalcontent-ui/gs-content-rating.c:232
#: libmalcontent-ui/gs-content-rating.c:480
msgid "Early Childhood"
msgstr ""

#. Translators: the placeholder is a user’s full name
#: libmalcontent-ui/restrict-applications-dialog.c:222
#, c-format
msgid "Restrict %s from using the following installed applications."
msgstr ""

#: libmalcontent-ui/restrict-applications-dialog.ui:6
#: libmalcontent-ui/restrict-applications-dialog.ui:12
msgid "Restrict Applications"
msgstr ""

#: libmalcontent-ui/restrict-applications-selector.ui:24
msgid "No applications found to restrict."
msgstr ""

#. Translators: this is the full name for an unknown user account.
#: libmalcontent-ui/user-controls.c:242 libmalcontent-ui/user-controls.c:253
msgid "unknown"
msgstr ""

#: libmalcontent-ui/user-controls.c:338 libmalcontent-ui/user-controls.c:425
#: libmalcontent-ui/user-controls.c:711
msgid "All Ages"
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:514
#, c-format
msgid ""
"Prevents %s from running web browsers. Limited web content may still be "
"available in other applications."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:519
#, c-format
msgid "Prevents specified applications from being used by %s."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:524
#, c-format
msgid "Prevents %s from installing applications."
msgstr ""

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:529
#, c-format
msgid "Applications installed by %s will not appear for other users."
msgstr ""

#: libmalcontent-ui/user-controls.ui:17
msgid "Application Usage Restrictions"
msgstr ""

#: libmalcontent-ui/user-controls.ui:68
msgid "Restrict _Web Browsers"
msgstr ""

#: libmalcontent-ui/user-controls.ui:152
msgid "_Restrict Applications"
msgstr ""

#: libmalcontent-ui/user-controls.ui:231
msgid "Software Installation Restrictions"
msgstr ""

#: libmalcontent-ui/user-controls.ui:281
msgid "Restrict Application _Installation"
msgstr ""

#: libmalcontent-ui/user-controls.ui:366
msgid "Restrict Application Installation for _Others"
msgstr ""

#: libmalcontent-ui/user-controls.ui:451
msgid "Application _Suitability"
msgstr ""

#: libmalcontent-ui/user-controls.ui:473
msgid ""
"Restricts browsing or installation of applications to applications suitable "
"for certain ages or above."
msgstr ""

#. Translators: This is the title of the main window
#. Translators: the name of the application as it appears in a software center
#: malcontent-control/application.c:105 malcontent-control/main.ui:12
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:9
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:3
msgid "Parental Controls"
msgstr ""

#: malcontent-control/application.c:270
msgid "Copyright © 2019, 2020 Endless Mobile, Inc."
msgstr ""

#. Translators: this should be "translated" to the
#. names of people who have translated Malcontent into
#. this language, one per line.
#: malcontent-control/application.c:275
msgid "translator-credits"
msgstr ""

#. Translators: "Malcontent" is the brand name of this
#. project, so should not be translated.
#: malcontent-control/application.c:281
msgid "Malcontent Website"
msgstr ""

#: malcontent-control/application.c:299
msgid "The help contents could not be displayed"
msgstr ""

#: malcontent-control/application.c:336
msgid "Failed to load user data from the system"
msgstr ""

#: malcontent-control/application.c:338
msgid "Please make sure that the AccountsService is installed and enabled."
msgstr ""

#: malcontent-control/carousel.ui:48
msgid "Previous Page"
msgstr ""

#: malcontent-control/carousel.ui:74
msgid "Next Page"
msgstr ""

#: malcontent-control/main.ui:93
msgid "Permission Required"
msgstr ""

#: malcontent-control/main.ui:107
msgid ""
"Permission is required to view and change user parental controls settings."
msgstr ""

#: malcontent-control/main.ui:148
msgid "No Child Users Configured"
msgstr ""

#: malcontent-control/main.ui:162
msgid ""
"No child users are currently set up on the system. Create one before setting "
"up their parental controls."
msgstr ""

#: malcontent-control/main.ui:174
msgid "Create _Child User"
msgstr ""

#: malcontent-control/main.ui:202
msgid "Loading…"
msgstr ""

#: malcontent-control/main.ui:265
msgid "_Help"
msgstr "_Ajuda"

#: malcontent-control/main.ui:269
msgid "_About Parental Controls"
msgstr ""

#. Translators: the brief summary of the application as it appears in a software center.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:12
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:4
msgid "Set parental controls and monitor usage by users"
msgstr ""

#. Translators: These are the application description paragraphs in the AppData file.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:16
msgid ""
"Manage users’ parental controls restrictions, controlling how long they can "
"use the computer for, what software they can install, and what installed "
"software they can run."
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:39
msgid "The GNOME Project"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:50
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:59
msgid "Minor improvements to parental controls application UI"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:51
msgid "Add a user manual"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:52
msgid "Translation updates"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:60
msgid "Translations to Ukrainian and Polish"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:67
msgid "Improve parental controls application UI and add icon"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:68
msgid "Support for indicating which accounts are parent accounts"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:75
msgid "Initial release of basic parental controls application"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:76
msgid "Support for setting app installation and run restrictions on users"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:83
msgid "Maintenance release of underlying parental controls library"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:7
msgid "org.freedesktop.MalcontentControl"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localise the semicolons! The list MUST also end with a semicolon!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:13
msgid ""
"parental controls;screen time;app restrictions;web browser restrictions;oars;"
"usage;usage limit;kid;child;"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:9
msgid "Manage parental controls"
msgstr ""

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:10
msgid "Authentication is required to read and change user parental controls"
msgstr ""

#: malcontent-control/user-selector.c:426
msgid "Your account"
msgstr "El vostre compte"

#. Always allow root, to avoid a situation where this PAM module prevents
#. * all users logging in with no way of recovery.
#: pam/pam_malcontent.c:142 pam/pam_malcontent.c:188
#, c-format
msgid "User ‘%s’ has no time limits enabled"
msgstr ""

#: pam/pam_malcontent.c:151 pam/pam_malcontent.c:172
#, c-format
msgid "Error getting session limits for user ‘%s’: %s"
msgstr ""

#: pam/pam_malcontent.c:182
#, c-format
msgid "User ‘%s’ has no time remaining"
msgstr ""

#: pam/pam_malcontent.c:200
#, c-format
msgid "Error setting time limit on login session: %s"
msgstr ""

#~ msgid "No cartoon violence"
#~ msgstr "Sense violència en personatges animats"

#~ msgid "Cartoon characters in unsafe situations"
#~ msgstr "Personatges animats en situacions insegures"

#~ msgid "Cartoon characters in aggressive conflict"
#~ msgstr "Personatges animats en conflictes agressius"

#~ msgid "Graphic violence involving cartoon characters"
#~ msgstr "Violència gràfica amb personatges animats"

#~ msgid "No fantasy violence"
#~ msgstr "Sense violència fantàstica"

#~ msgid "Characters in unsafe situations easily distinguishable from reality"
#~ msgstr ""
#~ "Personatges en situacions insegures fàcilment distingibles de la realitat"

#~ msgid ""
#~ "Characters in aggressive conflict easily distinguishable from reality"
#~ msgstr ""
#~ "Personatges en conflictes agressius fàcilment distingibles de la realitat"

#~ msgid "Graphic violence easily distinguishable from reality"
#~ msgstr "Violència gràfica fàcilment distingible de la realitat"

#~ msgid "No realistic violence"
#~ msgstr "Sense violència realista"

#~ msgid "Mildly realistic characters in unsafe situations"
#~ msgstr "Personatges mig reals en situacions insegures"

#~ msgid "Depictions of realistic characters in aggressive conflict"
#~ msgstr "Representacions de personatges realistes en conflictes agressius"

#~ msgid "Graphic violence involving realistic characters"
#~ msgstr "Violència gràfica amb personatges realistes"

#~ msgid "No bloodshed"
#~ msgstr "Sense matances"

#~ msgid "Unrealistic bloodshed"
#~ msgstr "Matances no realistes"

#~ msgid "Realistic bloodshed"
#~ msgstr "Matances realistes"

#~ msgid "Depictions of bloodshed and the mutilation of body parts"
#~ msgstr "Representacions de matances i mutilacions de parts del cos"

#~ msgid "No sexual violence"
#~ msgstr "Sense violència sexual"

#~ msgid "Rape or other violent sexual behavior"
#~ msgstr "Violació o altres comportaments sexuals violents"

#~ msgid "No references to alcohol"
#~ msgstr "Sense referències a l'alcohol"

#~ msgid "References to alcoholic beverages"
#~ msgstr "Referències a begudes alcohòliques"

#~ msgid "Use of alcoholic beverages"
#~ msgstr "Ús de begudes alcohòliques"

#~ msgid "No references to illicit drugs"
#~ msgstr "Sense referències a drogues il·legals"

#~ msgid "References to illicit drugs"
#~ msgstr "Referències a drogues il·legals"

#~ msgid "Use of illicit drugs"
#~ msgstr "Ús de drogues il·legals"

#~ msgid "References to tobacco products"
#~ msgstr "Referències als productes del tàbac"

#~ msgid "Use of tobacco products"
#~ msgstr "Ús de productes del tàbac"

#~ msgid "No nudity of any sort"
#~ msgstr "Sense nuesa de cap tipus"

#~ msgid "Brief artistic nudity"
#~ msgstr "Nuesa artística breu"

#~ msgid "Prolonged nudity"
#~ msgstr "Nuesa prolongada"

#~ msgid "No references or depictions of sexual nature"
#~ msgstr "Sense referències o representacions sexuals"

#~ msgid "Provocative references or depictions"
#~ msgstr "Referències o representacions provocatives"

#~ msgid "Sexual references or depictions"
#~ msgstr "Referències o representacions sexuals"

#~ msgid "Graphic sexual behavior"
#~ msgstr "Comportament sexual gràfic"

#~ msgid "No profanity of any kind"
#~ msgstr "Sense blasfèmia de cap tipus"

#~ msgid "Mild or infrequent use of profanity"
#~ msgstr "Ús lleu o poc freqüent de la blasfèmia"

#~ msgid "Moderate use of profanity"
#~ msgstr "Ús moderat de la blasfèmia"

#~ msgid "Strong or frequent use of profanity"
#~ msgstr "Ús extensiu o freqüent de la blasfèmia"

#~ msgid "No inappropriate humor"
#~ msgstr "Sense humor inapropiat"

#~ msgid "Slapstick humor"
#~ msgstr "Humor vulgar"

#~ msgid "Vulgar or bathroom humor"
#~ msgstr "Humor groller"

#~ msgid "Mature or sexual humor"
#~ msgstr "Humor adult o sexual"

#~ msgid "No discriminatory language of any kind"
#~ msgstr "Sense llenguatge discriminatori de cap tipus"

#~ msgid "Negativity towards a specific group of people"
#~ msgstr "Negatiu respecte a un grup específic de gent"

#~ msgid "Discrimination designed to cause emotional harm"
#~ msgstr "Discriminació pensada per causar dany emocional"

#~ msgid "Explicit discrimination based on gender, sexuality, race or religion"
#~ msgstr ""
#~ "Discriminació explicita basada en gènere, sexualitat, raça o religió"

#~ msgid "No advertising of any kind"
#~ msgstr "Sense publicitat de cap tipus"

#~ msgid "Product placement"
#~ msgstr "﻿Emplaçament de producte"

#~ msgid "Explicit references to specific brands or trademarked products"
#~ msgstr ""
#~ "Referències explícites a marques específiques o productes de marques "
#~ "registrades"

#~ msgid "No gambling of any kind"
#~ msgstr "Sense apostes de cap tipus"

#~ msgid "Gambling on random events using tokens or credits"
#~ msgstr "Apostes en esdeveniments aleatoris utilitzant testimonis o crèdits"

#~ msgid "Gambling using “play” money"
#~ msgstr "Apostes usant moneda virtual al joc"

#~ msgid "Gambling using real money"
#~ msgstr "Apostes usant diners reals"

#~ msgid "No ability to spend money"
#~ msgstr "Sense possibilitat de gastar diners"

#~ msgid "Ability to spend real money in-game"
#~ msgstr "Possibilitat de gastar diners de veritat al joc"

#~ msgid "No sharing of social network usernames or email addresses"
#~ msgstr ""
#~ "Sense compartició en les xarxes socials de noms d'usuari ni adreces de "
#~ "correu electrònic"

#~ msgid "Sharing social network usernames or email addresses"
#~ msgstr ""
#~ "Compartició en les xarxes socials de noms d'usuari o adreces de correu "
#~ "electrònic"

#~ msgid "No sharing of user information with 3rd parties"
#~ msgstr "No es comparteix informació de l'usuari amb terceres parts"

#~ msgid "No sharing of physical location to other users"
#~ msgstr "No es comparteix la ubicació física amb altres usuaris"

#~ msgid "Sharing physical location to other users"
#~ msgstr "Compartició la ubicació física amb altres usuaris"
