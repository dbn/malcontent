# Italian translations for malcontent package.
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the malcontent package.
# Milo Casagrande <milo@milo.name>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: malcontent\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pwithnall/malcontent/"
"issues\n"
"POT-Creation-Date: 2020-09-10 03:28+0000\n"
"PO-Revision-Date: 2020-09-13 14:25+0200\n"
"Last-Translator: Milo Casagrande <milo@milo.name>\n"
"Language-Team: none\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#: accounts-service/com.endlessm.ParentalControls.policy.in:4
msgid "Change your own app filter"
msgstr "Modifica filtri app personali"

#: accounts-service/com.endlessm.ParentalControls.policy.in:5
msgid "Authentication is required to change your app filter."
msgstr "È richiesto autenticarsi per modificare i propri filtri app."

#: accounts-service/com.endlessm.ParentalControls.policy.in:14
msgid "Read your own app filter"
msgstr "Lettura dei filtri app personali"

#: accounts-service/com.endlessm.ParentalControls.policy.in:15
msgid "Authentication is required to read your app filter."
msgstr "È richiesto autenticarsi per leggere i propri filtri app."

#: accounts-service/com.endlessm.ParentalControls.policy.in:24
msgid "Change another user’s app filter"
msgstr "Modifica dei filtri app di un altro utente"

#: accounts-service/com.endlessm.ParentalControls.policy.in:25
msgid "Authentication is required to change another user’s app filter."
msgstr ""
"È richiesto autenticarsi per modificare i filtri app di un altro utente."

#: accounts-service/com.endlessm.ParentalControls.policy.in:34
msgid "Read another user’s app filter"
msgstr "Lettura dei filtri app di un altro utente"

#: accounts-service/com.endlessm.ParentalControls.policy.in:35
msgid "Authentication is required to read another user’s app filter."
msgstr "È richiesto autenticarsi per leggere i filtri app di un altro utente."

#: accounts-service/com.endlessm.ParentalControls.policy.in:44
msgid "Change your own session limits"
msgstr "Modifica dei limiti di sessione personali"

#: accounts-service/com.endlessm.ParentalControls.policy.in:45
msgid "Authentication is required to change your session limits."
msgstr "È richiesto autenticarsi per modificare i propri limiti di sessione."

#: accounts-service/com.endlessm.ParentalControls.policy.in:54
msgid "Read your own session limits"
msgstr "Lettura limiti di sessione personali"

#: accounts-service/com.endlessm.ParentalControls.policy.in:55
msgid "Authentication is required to read your session limits."
msgstr "È richiesto autenticarsi per leggere i propri limiti di sessione."

#: accounts-service/com.endlessm.ParentalControls.policy.in:64
msgid "Change another user’s session limits"
msgstr "Modifica dei limiti di sessione di un altro utente"

#: accounts-service/com.endlessm.ParentalControls.policy.in:65
msgid "Authentication is required to change another user’s session limits."
msgstr ""
"È richiesto autenticarsi per modificare i limiti di sessione di un altro "
"utente."

#: accounts-service/com.endlessm.ParentalControls.policy.in:74
msgid "Read another user’s session limits"
msgstr "Lettura dei limiti di sessione di un altro utente"

#: accounts-service/com.endlessm.ParentalControls.policy.in:75
msgid "Authentication is required to read another user’s session limits."
msgstr ""
"È richiesto autenticarsi per leggere i limiti di sessione di un altro utente."

#: accounts-service/com.endlessm.ParentalControls.policy.in:84
msgid "Change your own account info"
msgstr "Modifica delle informazioni dell'account personale"

#: accounts-service/com.endlessm.ParentalControls.policy.in:85
msgid "Authentication is required to change your account info."
msgstr ""
"È richiesto autenticarsi per modificare le informazioni del proprio account."

#: accounts-service/com.endlessm.ParentalControls.policy.in:94
msgid "Read your own account info"
msgstr "Lettura delle informazioni dell'account personale"

#: accounts-service/com.endlessm.ParentalControls.policy.in:95
msgid "Authentication is required to read your account info."
msgstr ""
"È richiesto autenticarsi per leggere le informazioni del proprio account."

#: accounts-service/com.endlessm.ParentalControls.policy.in:104
msgid "Change another user’s account info"
msgstr "Modifica delle informazioni dell'account di un altro utente"

#: accounts-service/com.endlessm.ParentalControls.policy.in:105
msgid "Authentication is required to change another user’s account info."
msgstr ""
"È richiesto autenticarsi per modificare le informazioni dell'account di un "
"altro utente."

#: accounts-service/com.endlessm.ParentalControls.policy.in:114
msgid "Read another user’s account info"
msgstr "Lettura delle informazioni dell'account di un altro utente"

#: accounts-service/com.endlessm.ParentalControls.policy.in:115
msgid "Authentication is required to read another user’s account info."
msgstr ""
"È richiesto autenticarsi per leggere le informazioni dell'account di un "
"altro utente."

#: libmalcontent/app-filter.c:694
#, c-format
msgid "App filter for user %u was in an unrecognized format"
msgstr "Il filtro app per l'utente %u era in un formato non riconosciuto"

#: libmalcontent/app-filter.c:725
#, c-format
msgid "OARS filter for user %u has an unrecognized kind ‘%s’"
msgstr "Il filtro OARS per l'utente %u presenta un tipo «%s» non riconosciuto"

#: libmalcontent/manager.c:283 libmalcontent/manager.c:412
#, c-format
msgid "Not allowed to query app filter data for user %u"
msgstr "Non è consentito richiedere i dati dei filtri app per l'utente %u"

#: libmalcontent/manager.c:288
#, c-format
msgid "User %u does not exist"
msgstr "L'utente %u non esiste"

#: libmalcontent/manager.c:394
msgid "App filtering is globally disabled"
msgstr "I filtri sulle app sono disabilitati globalmente"

#: libmalcontent/manager.c:777
msgid "Session limits are globally disabled"
msgstr "I limiti sulla sessione sono disabilitati globalmente"

#: libmalcontent/manager.c:795
#, c-format
msgid "Not allowed to query session limits data for user %u"
msgstr ""
"Non è consentito richiedere i dati dei limiti di sessione per l'utente %u"

#: libmalcontent/session-limits.c:306
#, c-format
msgid "Session limit for user %u was in an unrecognized format"
msgstr ""
"Il limite di sessione per l'utente %u era in un formato non riconosciuto"

#: libmalcontent/session-limits.c:328
#, c-format
msgid "Session limit for user %u has an unrecognized type ‘%u’"
msgstr ""
"Il limite di sessione per l'utente %u presenta un tipo «%u» non riconosciuto"

#: libmalcontent/session-limits.c:346
#, c-format
msgid "Session limit for user %u has invalid daily schedule %u–%u"
msgstr ""
"Il limite di sessione per l'utente %u presenta una pianificazione "
"giornaliera %u-%u non valida"

#. TRANSLATORS: This is the formatting of English and localized name
#. of the rating e.g. "Adults Only (solo adultos)"
#: libmalcontent-ui/gs-content-rating.c:75
#, c-format
msgid "%s (%s)"
msgstr "%2$s (%1$s)"

#: libmalcontent-ui/gs-content-rating.c:209
msgid "General"
msgstr "Generale"

#: libmalcontent-ui/gs-content-rating.c:218
msgid "ALL"
msgstr "Tutto"

#: libmalcontent-ui/gs-content-rating.c:222
#: libmalcontent-ui/gs-content-rating.c:485
msgid "Adults Only"
msgstr "Solo per adulti"

#: libmalcontent-ui/gs-content-rating.c:224
#: libmalcontent-ui/gs-content-rating.c:484
msgid "Mature"
msgstr "Maturo"

#: libmalcontent-ui/gs-content-rating.c:226
#: libmalcontent-ui/gs-content-rating.c:483
msgid "Teen"
msgstr "Adolescente"

#: libmalcontent-ui/gs-content-rating.c:228
#: libmalcontent-ui/gs-content-rating.c:482
msgid "Everyone 10+"
msgstr "Chiunque di 10+"

#: libmalcontent-ui/gs-content-rating.c:230
#: libmalcontent-ui/gs-content-rating.c:481
msgid "Everyone"
msgstr "Chiunque"

#: libmalcontent-ui/gs-content-rating.c:232
#: libmalcontent-ui/gs-content-rating.c:480
msgid "Early Childhood"
msgstr "Prima infanzia"

#. Translators: the placeholder is a user’s full name
#: libmalcontent-ui/restrict-applications-dialog.c:222
#, c-format
msgid "Restrict %s from using the following installed applications."
msgstr "Limita l'uso delle seguenti applicazioni da parte dell'utente %s."

#: libmalcontent-ui/restrict-applications-dialog.ui:6
#: libmalcontent-ui/restrict-applications-dialog.ui:12
msgid "Restrict Applications"
msgstr "Limita applicazioni"

#: libmalcontent-ui/restrict-applications-selector.ui:24
msgid "No applications found to restrict."
msgstr "Non è stata trovata alcuna applicazione da limitare."

#. Translators: this is the full name for an unknown user account.
#: libmalcontent-ui/user-controls.c:242 libmalcontent-ui/user-controls.c:253
msgid "unknown"
msgstr "sconosciuto"

#: libmalcontent-ui/user-controls.c:338 libmalcontent-ui/user-controls.c:425
#: libmalcontent-ui/user-controls.c:711
msgid "All Ages"
msgstr "Tutte le età"

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:514
#, c-format
msgid ""
"Prevents %s from running web browsers. Limited web content may still be "
"available in other applications."
msgstr ""
"Impedisce all'utente %s di lanciare browser web. Contenuti web limitati "
"possono comunque essere accessibili in altre applicazioni."

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:519
#, c-format
msgid "Prevents specified applications from being used by %s."
msgstr "Impedisce all'utente %s di utilizzare applicazioni specifiche."

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:524
#, c-format
msgid "Prevents %s from installing applications."
msgstr "Impedisce all'utente %s di installare applicazioni."

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:529
#, c-format
msgid "Applications installed by %s will not appear for other users."
msgstr ""
"Le applicazioni installate da %s non saranno visualizzate da altri utenti."

#: libmalcontent-ui/user-controls.ui:17
msgid "Application Usage Restrictions"
msgstr "Limitazioni utilizzo applicazioni"

#: libmalcontent-ui/user-controls.ui:68
msgid "Restrict _Web Browsers"
msgstr "Limita browser _web"

#: libmalcontent-ui/user-controls.ui:152
msgid "_Restrict Applications"
msgstr "_Limita applicazioni"

#: libmalcontent-ui/user-controls.ui:231
msgid "Software Installation Restrictions"
msgstr "Limitazioni installazione software"

#: libmalcontent-ui/user-controls.ui:281
msgid "Restrict Application _Installation"
msgstr "Limita _installazione applicazioni"

#: libmalcontent-ui/user-controls.ui:366
msgid "Restrict Application Installation for _Others"
msgstr "Limita installazione applicazioni ad _altri"

#: libmalcontent-ui/user-controls.ui:451
msgid "Application _Suitability"
msgstr "Co_mpatibilità applicazioni"

#: libmalcontent-ui/user-controls.ui:473
msgid ""
"Restricts browsing or installation of applications to applications suitable "
"for certain ages or above."
msgstr ""
"Limita l'esplorazione o l'installazione di applicazioni a solo quelle "
"compatibili con certe fasce d'età."

#. Translators: This is the title of the main window
#. Translators: the name of the application as it appears in a software center
#: malcontent-control/application.c:105 malcontent-control/main.ui:12
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:9
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:3
msgid "Parental Controls"
msgstr "Controlli parentali"

#: malcontent-control/application.c:270
msgid "Copyright © 2019, 2020 Endless Mobile, Inc."
msgstr "Copyright © 2019, 2020 Endless Mobile, Inc."

#. Translators: this should be "translated" to the
#. names of people who have translated Malcontent into
#. this language, one per line.
#: malcontent-control/application.c:275
msgid "translator-credits"
msgstr "Milo Casagrande <milo@milo.name>, 2020"

#. Translators: "Malcontent" is the brand name of this
#. project, so should not be translated.
#: malcontent-control/application.c:281
msgid "Malcontent Website"
msgstr "Sito web di Malcontent"

#: malcontent-control/application.c:299
msgid "The help contents could not be displayed"
msgstr "I contenuti dell'aiuto non possono essere visualizzati"

#: malcontent-control/application.c:336
msgid "Failed to load user data from the system"
msgstr "Caricamento dei dati utente dal sistema non riuscito"

#: malcontent-control/application.c:338
msgid "Please make sure that the AccountsService is installed and enabled."
msgstr ""
"Assicurarsi che il servizio AccountsService sia installato e abilitato."

#: malcontent-control/carousel.ui:48
msgid "Previous Page"
msgstr "Pagina precedente"

#: malcontent-control/carousel.ui:74
msgid "Next Page"
msgstr "Pagina successiva"

#: malcontent-control/main.ui:93
msgid "Permission Required"
msgstr "Richiesti permessi"

#: malcontent-control/main.ui:107
msgid ""
"Permission is required to view and change user parental controls settings."
msgstr ""
"È richiesto avere i permessi necessari per visualizzare e modificare i "
"controlli parentali dell'utente."

#: malcontent-control/main.ui:148
msgid "No Child Users Configured"
msgstr "Nessun utente bambino configurato"

#: malcontent-control/main.ui:162
msgid ""
"No child users are currently set up on the system. Create one before setting "
"up their parental controls."
msgstr ""
"Nessun utente bambino è configurato nel sistema. Crearne uno prima di "
"impostarne i controlli parentali."

#: malcontent-control/main.ui:174
msgid "Create _Child User"
msgstr "Crea utente _bambino"

#: malcontent-control/main.ui:202
msgid "Loading…"
msgstr "Caricamento…"

#: malcontent-control/main.ui:265
msgid "_Help"
msgstr "A_iuto"

#: malcontent-control/main.ui:269
msgid "_About Parental Controls"
msgstr "I_nformazioni su Controlli parentali"

#. Translators: the brief summary of the application as it appears in a software center.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:12
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:4
msgid "Set parental controls and monitor usage by users"
msgstr "Imposta i controlli parentali e monitora l'utilizzo degli utenti"

#. Translators: These are the application description paragraphs in the AppData file.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:16
msgid ""
"Manage users’ parental controls restrictions, controlling how long they can "
"use the computer for, what software they can install, and what installed "
"software they can run."
msgstr ""
"Gestisce i limiti sui controlli parentali dell'utente, controllando per "
"quanto tempo possono usare il computer, che software possono installare e "
"quali applicazioni installate possono usare."

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:25
msgid "Main window"
msgstr "Finestra principale"

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:38
msgid "The GNOME Project"
msgstr "Il progetto GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localise the semicolons! The list MUST also end with a semicolon!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:13
msgid ""
"parental controls;screen time;app restrictions;web browser restrictions;oars;"
"usage;usage limit;kid;child;"
msgstr ""
"controlli parentali;tempo schermo;tempo utilizzo;limitazione app;limitazioni "
"browser web;utilizzo;limite utilizzo;utilizzazione;bambino;bambina;ragazzo;"
"ragazza;"

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:9
msgid "Manage parental controls"
msgstr "Gestione controlli parentali"

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:10
msgid "Authentication is required to read and change user parental controls"
msgstr ""
"È richiesto autenticarsi per leggere e modificare i controlli parentali"

#: malcontent-control/user-selector.c:426
msgid "Your account"
msgstr "Il proprio account"

#. Always allow root, to avoid a situation where this PAM module prevents
#. * all users logging in with no way of recovery.
#: pam/pam_malcontent.c:142 pam/pam_malcontent.c:188
#, c-format
msgid "User ‘%s’ has no time limits enabled"
msgstr "L'utente «%s» non ha alcun limite di tempo abilitato"

#: pam/pam_malcontent.c:151 pam/pam_malcontent.c:172
#, c-format
msgid "Error getting session limits for user ‘%s’: %s"
msgstr "Errore nell'ottenere i limiti di sessione per l'utente «%s»: %s"

#: pam/pam_malcontent.c:182
#, c-format
msgid "User ‘%s’ has no time remaining"
msgstr "L'utente «%s» non ha più tempo disponibile"

#: pam/pam_malcontent.c:200
#, c-format
msgid "Error setting time limit on login session: %s"
msgstr "Errore nell'impostare il tempo limite sulla sessione di accesso: %s"
